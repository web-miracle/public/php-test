<?php

require __DIR__ . '/../vendor/autoload.php';

header('Content-Type: application/json');


/********************************************************
 *  Можно выполнять любые действия                      *
 *  Класс Builder обращаться как \App\Builder           *
 *  Чтобы увидеть ответ оберните данные в `json_encode` *
 *  и выведите через echo или print                     *
 ********************************************************/

echo json_encode([
  "json" => "data"
]);
