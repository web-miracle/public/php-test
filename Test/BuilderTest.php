<?php

namespace Test;

use App\Builder;
use App\Child;
use PHPUnit\Framework\TestCase;

class BuilderTest extends TestCase
{
    /**
     * @group count
     */
    public function testCount()
    {
        $child_1 = [
            "id" => 1,
            "name" => "Павел Мишалов",
            "age" => 15,
            "school" => "Лицей номер 14",
        ];
        $child_2 = [
            "id" => 2,
            "name" => "Дмитрий Новиков",
            "age" => 17,
            "school" => "Школа им. Гагарина",
        ];
        $child_3 = [
            "id" => 3,
            "name" => "Василий Данилишин",
            "age" => 13,
            "school" => "Лицей номер 14",
        ];
        $child_4 = [
            "id" => 4,
            "name" => "Владислав Ткаченко",
            "age" => 17,
            "school" => "Школа им. Ленина",
        ];

        $builder = new Builder([
            new Child($child_1),
            new Child($child_2),
            new Child($child_3),
            new Child($child_4),
        ]);
        $result = $builder->count();

        $this->assertEquals(4, $result, 'Функция count не правильно подсчитала количество');
    }

    /**
     * @group like
     */
    public function testLike()
    {
        $child_1 = [
            "id" => 1,
            "name" => "Павел Мишалов",
            "age" => 15,
            "school" => "Лицей номер 14",
        ];
        $child_2 = [
            "id" => 2,
            "name" => "Дмитрий Новиков",
            "age" => 17,
            "school" => "Школа им. Гагарина",
        ];
        $child_3 = [
            "id" => 3,
            "name" => "Василий Данилишин",
            "age" => 13,
            "school" => "Лицей номер 14",
        ];
        $child_4 = [
            "id" => 4,
            "name" => "Владислав Ткаченко",
            "age" => 17,
            "school" => "Школа им. Ленина",
        ];

        $builder = new Builder([
            new Child($child_1),
            new Child($child_2),
            new Child($child_3),
            new Child($child_4),
        ]);
        $result = $builder->like("school", "Лицей")->toResponse();

        $this->assertEquals(
            [
                ["id" => 1, "name" => "Павел Мишалов", "age" => 15, "school" => "Лицей номер 14"],
                ["id" => 3, "name" => "Василий Данилишин", "age" => 13, "school" => "Лицей номер 14"],
            ],
            $result,
            'Функция like не правильно поиск в полях'
        );
    }

    /**
     * @group select
     */
    public function testSelect()
    {
        $child_1 = [
            "id" => 1,
            "name" => "Павел Мишалов",
            "age" => 15,
            "school" => "Лицей номер 14",
        ];
        $child_2 = [
            "id" => 2,
            "name" => "Дмитрий Новиков",
            "age" => 17,
            "school" => "Школа им. Гагарина",
        ];
        $child_3 = [
            "id" => 3,
            "name" => "Василий Данилишин",
            "age" => 13,
            "school" => "Лицей номер 14",
        ];
        $child_4 = [
            "id" => 4,
            "name" => "Владислав Ткаченко",
            "age" => 17,
            "school" => "Школа им. Ленина",
        ];

        $builder = new Builder([
            new Child($child_1),
            new Child($child_2),
            new Child($child_3),
            new Child($child_4),
        ]);
        $result = $builder->select('name', 'age')->toResponse();

        $this->assertEquals(
            [
                ["name" => "Павел Мишалов", "age" => 15],
                ["name" => "Дмитрий Новиков", "age" => 17],
                ["name" => "Василий Данилишин", "age" => 13],
                ["name" => "Владислав Ткаченко", "age" => 17],
            ],
            $result,
            'Функция select не правильно преобразовалло данные'
        );
    }

    /**
     * @group where
     */
    public function testWhere()
    {
        $child_1 = [
            "id" => 1,
            "name" => "Павел Мишалов",
            "age" => 15,
            "school" => "Лицей номер 14",
        ];
        $child_2 = [
            "id" => 2,
            "name" => "Дмитрий Новиков",
            "age" => 17,
            "school" => "Школа им. Гагарина",
        ];
        $child_3 = [
            "id" => 3,
            "name" => "Василий Данилишин",
            "age" => 13,
            "school" => "Лицей номер 14",
        ];
        $child_4 = [
            "id" => 4,
            "name" => "Владислав Ткаченко",
            "age" => 17,
            "school" => "Школа им. Ленина",
        ];

        $builder = new Builder([
            new Child($child_1),
            new Child($child_2),
            new Child($child_3),
            new Child($child_4),
        ]);
        $result = $builder->where('age', 17)->toResponse();

        $this->assertEquals(
            [
                ["id" => 2, "name" => "Дмитрий Новиков", "age" => 17, "school" => "Школа им. Гагарина"],
                ["id" => 4, "name" => "Владислав Ткаченко", "age" => 17, "school" => "Школа им. Ленина"],
            ],
            $result,
            'Функция where не правильно отфильтровало данные'
        );
    }

    /**
     * @group combine
     */
    public function testCombine()
    {
        $child_1 = [
            "id" => 1,
            "name" => "Павел Мишалов",
            "age" => 15,
            "school" => "Лицей номер 14",
        ];
        $child_2 = [
            "id" => 2,
            "name" => "Дмитрий Новиков",
            "age" => 17,
            "school" => "Школа им. Гагарина",
        ];
        $child_3 = [
            "id" => 3,
            "name" => "Василий Данилишин",
            "age" => 13,
            "school" => "Лицей номер 14",
        ];
        $child_4 = [
            "id" => 4,
            "name" => "Владислав Ткаченко",
            "age" => 17,
            "school" => "Школа им. Ленина",
        ];

        $builder = new Builder([
            new Child($child_1),
            new Child($child_2),
            new Child($child_3),
            new Child($child_4),
        ]);
        $builder = $builder->where('age', 17)->like('school', 'Гагарина')->select('id', 'name');
        $result = $builder->toResponse();
        $resultCount = $builder->count();

        $this->assertEquals(
            [
                ["id" => 2, "name" => "Дмитрий Новиков"],
            ],
            $result,
            'Комбинирование методов не правильно отфильтровало данные'
        );

        $this->assertEquals(
            1,
            $resultCount,
            'Комбинирование методов не правильно отфильтровало данные'
        );
    }
}