<?php

namespace App;

use App\Child;

class Builder {

    /**
     * Фиксирует результат данных
     * @var mixed
     */
    private $attributes;

    /**
     * Builder constructor.
     * @param Child[] $children
     */
    public function __construct(array $children)
    {
        // В начальной структуре преобразовываем все поля к массиву
        $this->attributes = array_map(
            function (Child $item) {
                return $item->toArray();
            },
            $children
        );
    }

    /**
     * Оставляет только указанные поля объектов
     *
     * {@example \App\Example\select.php}
     *
     * @param mixed ...$fields
     * @return $this
     */
    public function select(array ...$fields): self
    {
        return $this;
    }

    /**
     * Оставляет только данные, у которых проходит фильтрация, где поле соответствует значению
     *
     * {@example \App\Example\where.php}
     *
     * @param string $field Поле, по которому фильтруем
     * @param mixed $arg Значение, с которым сравниваем
     * @return $this
     */
    public function where(string $field, $arg): self
    {
        return $this;
    }

    /**
     * Оставляет только данные, у которых есть вхождение подстроки, где поле содержит значение значению
     *
     * {@example \App\Example\like.php}
     *
     * @param string $field Поле, по которому фильтруем
     * @param mixed $arg Значение, с которым сравниваем
     * @return $this
     */
    public function like(string $field, $arg): self
    {
        return $this;
    }

    /**
     * Возвращает количество данных
     *
     * {@example \App\Example\count.php}
     *
     * @return int
     */
    public function count(): int
    {
        return 0;
    }

    /**
     * Возвращает результат обработки данных
     *
     * @return mixed
     */
    function toResponse()
    {
        return $this->attributes;
    }
}
