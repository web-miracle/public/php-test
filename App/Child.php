<?php

namespace App;

class Child
{
    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var number */
    public $age;

    /** @var string */
    public $school;

    public function __construct(array $options)
    {
        [
            "id" => $id,
            "name" => $name,
            "age" => $age,
            "school" => $school,
        ] = $options;

        $this->id = $id;
        $this->name = $name;
        $this->age = $age;
        $this->school = $school;
    }

    /**
     * Преобразовывает объект к массиву
     * @return array
     */
    public function toArray(): array
    {
        return [
            "id" => $this->id,
            "name" => $this->name,
            "age" => $this->age,
            "school" => $this->school,
        ];
    }
}